package githubpop.natanximenes.com.br.githubpop.utils;

/**
 * Created by natanximenes on 9/28/16.
 */

public interface PullClickListener {
    void onPullClickListener(String url);
}
