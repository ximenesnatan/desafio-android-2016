package githubpop.natanximenes.com.br.githubpop.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by natanximenes on 9/21/16.
 */
public class Repository implements Parcelable {
    public static final Creator<Repository> CREATOR = new Creator<Repository>() {
        @Override
        public Repository createFromParcel(Parcel in) {
            return new Repository(in);
        }

        @Override
        public Repository[] newArray(int size) {
            return new Repository[size];
        }
    };
    private int id;
    private String name;
    private Owner owner;
    private String description;
    @SerializedName("stargazers_count")
    private int stargazersCount;
    @SerializedName("forks_count")
    private int forksCount;

    protected Repository(Parcel in) {
        id = in.readInt();
        name = in.readString();
        description = in.readString();
        stargazersCount = in.readInt();
        forksCount = in.readInt();
        owner = (Owner) in.readSerializable();
    }

    public Repository() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Owner getOwner() {
        return owner;
    }

    public int getStargazersCount() {
        return stargazersCount;
    }

    public int getForksCount() {
        return forksCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeInt(stargazersCount);
        parcel.writeInt(forksCount);
        parcel.writeSerializable(owner);
    }
}
