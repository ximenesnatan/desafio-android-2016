package githubpop.natanximenes.com.br.githubpop.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import githubpop.natanximenes.com.br.githubpop.R;
import githubpop.natanximenes.com.br.githubpop.model.Repository;
import githubpop.natanximenes.com.br.githubpop.utils.RepoClickListener;

/**
 * Created by natanximenes on 9/22/16.
 */
public class RepoListAdapter extends RecyclerView.Adapter<RepoListAdapter.ViewHolder> {
    List<Repository> repos;
    Context context;
    RepoClickListener repoClickListener;

    public RepoListAdapter(List<Repository> repos, Context context, RepoClickListener repoClickListener) {
        this.repos = repos;
        this.context = context;
        this.repoClickListener = repoClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.repo_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Repository repo = repos.get(position);
        if (repo.getOwner().getName() != null) {
            String ownerName = String.format(context.getString(R.string.user_full_name), repo.getOwner().getName());
            holder.ownerFullUserName.setText(ownerName);
            holder.ownerFullUserName.setVisibility(View.VISIBLE);
        }
        Picasso.with(context)
                .load(repo.getOwner().getAvatarUrl())
                .placeholder(R.mipmap.ic_launcher)
                .fit()
                .into(holder.ownerAvatar);
        holder.ownerLogin.setText(repo.getOwner().getLogin());
        holder.repoDescription.setText(repo.getDescription());
        holder.repoTitle.setText(repo.getName());
        holder.repoForks.setText(String.valueOf(repo.getForksCount()));
        holder.repoStars.setText(String.valueOf(repo.getStargazersCount()));
        holder.repoClickListener = repoClickListener;
        holder.owner = repo.getOwner().getLogin();
        holder.repoName = repo.getName();
    }


    @Override
    public int getItemCount() {
        return repos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.repoItem_user_avatar)
        ImageView ownerAvatar;
        @BindView(R.id.repoItem_repo_title)
        TextView repoTitle;
        @BindView(R.id.repoItem_repo_description)
        TextView repoDescription;
        @BindView(R.id.repoItem_forks)
        TextView repoForks;
        @BindView(R.id.repoItem_stars)
        TextView repoStars;
        @BindView(R.id.repoItem_nickname)
        TextView ownerLogin;
        @BindView(R.id.repoItem_full_user_name)
        TextView ownerFullUserName;
        @BindView(R.id.repoItem_card)
        CardView card;
        RepoClickListener repoClickListener;
        String repoName;
        String owner;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.repoItem_card)
        public void repoClicked() {
            repoClickListener.onClickRepoListener(owner, repoName);
        }

    }
}
