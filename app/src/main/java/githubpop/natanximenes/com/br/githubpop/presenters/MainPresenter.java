package githubpop.natanximenes.com.br.githubpop.presenters;

/**
 * Created by natanximenes on 9/22/16.
 */

public interface MainPresenter {
    void searchPopRepos(int page);
    void attachToView();
    void detachFromView();
}
