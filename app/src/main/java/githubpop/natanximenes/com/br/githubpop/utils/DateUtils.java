package githubpop.natanximenes.com.br.githubpop.utils;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

;


/**
 * Created by natanximenes on 9/26/16.
 */

public class DateUtils {

    public static String formatDate(String date) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.US);
        Date myDate = null;
        try {
            myDate = inputFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formatedDate = outputFormat.format(myDate);
        return formatedDate;
    }
}
