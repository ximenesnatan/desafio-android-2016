package githubpop.natanximenes.com.br.githubpop.views;

import java.util.List;

import githubpop.natanximenes.com.br.githubpop.model.Pull;

/**
 * Created by natanximenes on 9/26/16.
 */
public interface PullRequestsView {
    void showProgress();

    void hideProgress();

    void showPulls(List<Pull> pulls);

    void showRetryOption();
}
