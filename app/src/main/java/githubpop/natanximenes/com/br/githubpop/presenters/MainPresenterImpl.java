package githubpop.natanximenes.com.br.githubpop.presenters;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import githubpop.natanximenes.com.br.githubpop.events.RepositoriesEvent;
import githubpop.natanximenes.com.br.githubpop.events.UnexpectedErrorEvent;
import githubpop.natanximenes.com.br.githubpop.interactors.GithubPopReposInteractor;
import githubpop.natanximenes.com.br.githubpop.interactors.GithubPopReposInteractorImpl;
import githubpop.natanximenes.com.br.githubpop.views.MainView;

/**
 * Created by natanximenes on 9/22/16.
 */
public class MainPresenterImpl implements MainPresenter {
    GithubPopReposInteractor githubPopReposInteractor;
    MainView mainView;
    EventBus bus;

    public MainPresenterImpl(MainView mainView) {
        this.mainView = mainView;
        githubPopReposInteractor = new GithubPopReposInteractorImpl();
        bus = EventBus.getDefault();
    }

    public MainPresenterImpl() {

    }

    @Override
    public void attachToView() {
        if (!bus.isRegistered(this)) {
            bus.register(this);
        }
    }

    @Override
    public void detachFromView() {
        bus.unregister(this);
    }

    @Override
    public void searchPopRepos(int page) {
        mainView.showProgress();
        githubPopReposInteractor.findPopRepositories(page);
    }

    @Subscribe
    public void onEvent(RepositoriesEvent repositoriesEvent) {
        mainView.hideProgress();
        mainView.showRepos(repositoriesEvent.getRepositories());
    }

    @Subscribe
    public void onEvent(UnexpectedErrorEvent errorEvent) {
        mainView.hideProgress();
        mainView.showRetryOption();
    }

}
