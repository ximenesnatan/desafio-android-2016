package githubpop.natanximenes.com.br.githubpop.service;

import githubpop.natanximenes.com.br.githubpop.model.SearchResult;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by natanximenes on 9/22/16.
 */

public interface GithubRepoService {

    @GET("search/repositories?q=language:Java&sort=stars")
    Call<SearchResult> showPopRepos(@Query("page") int page);
}
