package githubpop.natanximenes.com.br.githubpop.service;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import githubpop.natanximenes.com.br.githubpop.BuildConfig;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by natanximenes on 9/22/16.
 */

public class Api {
    private final static String BASE_URL = "https://api.github.com/";
    private static Api api;
    private Retrofit retrofit;

    private Api() {
        this.retrofit = getRetrofit();
    }

    public static Api getApiInstance() {
        if (api == null) {
            api = new Api();
            return api;
        }
        return api;
    }

    public Retrofit getApi() {
        return retrofit;
    }

    private Retrofit getRetrofit() {
        OkHttpClient.Builder client = new OkHttpClient().newBuilder();
        client.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request originalRequest = chain.request();
                originalRequest = originalRequest.newBuilder()
                        .addHeader("Accept", "application/vnd.github.v3+json")
                        .build();

                return chain.proceed(originalRequest);
            }
        });
        client.connectTimeout(15, TimeUnit.SECONDS);
        client.readTimeout(20, TimeUnit.SECONDS);
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            client.interceptors().add(logging);
        }

        OkHttpClient clientOk = client.build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(clientOk)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

}
