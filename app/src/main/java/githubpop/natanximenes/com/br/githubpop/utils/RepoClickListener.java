package githubpop.natanximenes.com.br.githubpop.utils;

/**
 * Created by natanximenes on 9/27/16.
 */

public interface RepoClickListener {
    void onClickRepoListener(String owner, String repoName);
}
