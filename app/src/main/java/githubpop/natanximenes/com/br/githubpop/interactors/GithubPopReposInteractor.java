package githubpop.natanximenes.com.br.githubpop.interactors;

/**
 * Created by natanximenes on 9/22/16.
 */

public interface GithubPopReposInteractor {
    void findPopRepositories(int page);
}
