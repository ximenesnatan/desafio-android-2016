package githubpop.natanximenes.com.br.githubpop.events;

import java.util.List;

import githubpop.natanximenes.com.br.githubpop.model.Repository;

/**
 * Created by natanximenes on 9/22/16.
 */

public class RepositoriesEvent {
    List<Repository> repositories;

    public List<Repository> getRepositories() {
        return repositories;
    }

    public RepositoriesEvent(List<Repository> repositories) {
        this.repositories = repositories;
    }
}
