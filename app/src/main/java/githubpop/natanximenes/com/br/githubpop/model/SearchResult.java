package githubpop.natanximenes.com.br.githubpop.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by natanximenes on 9/21/16.
 */

public class SearchResult {
    @SerializedName("items")
    private List<Repository> repositories;

    public List<Repository> getRepositories() {
        return repositories;
    }
}
