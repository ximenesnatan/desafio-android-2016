package githubpop.natanximenes.com.br.githubpop.service;

import java.util.List;

import githubpop.natanximenes.com.br.githubpop.model.Pull;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by natanximenes on 9/26/16.
 */

public interface GithubPullRequestService {
    @GET("repos/{owner}/{repo}/pulls")
    Call<List<Pull>> getRepoPulls(@Path("owner") String owner, @Path("repo") String repo,
                                  @Query("page") int page);
}
