package githubpop.natanximenes.com.br.githubpop.events;

import java.util.List;

import githubpop.natanximenes.com.br.githubpop.model.Pull;

/**
 * Created by natanximenes on 9/26/16.
 */
public class PullRequestsEvent {
    List<Pull> pulls;

    public PullRequestsEvent(List<Pull> pulls) {
        this.pulls = pulls;
    }

    public List<Pull> getPulls() {
        return pulls;
    }
}
