package githubpop.natanximenes.com.br.githubpop.views;

import java.util.List;

import githubpop.natanximenes.com.br.githubpop.model.Repository;

/**
 * Created by natanximenes on 9/22/16.
 */
public interface MainView {
    void showProgress();

    void hideProgress();

    void showRepos(List<Repository> repos);

    void showRetryOption();
}
