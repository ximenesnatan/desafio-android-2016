package githubpop.natanximenes.com.br.githubpop.views;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import githubpop.natanximenes.com.br.githubpop.R;
import githubpop.natanximenes.com.br.githubpop.adapters.PullRequestsAdapter;
import githubpop.natanximenes.com.br.githubpop.model.Pull;
import githubpop.natanximenes.com.br.githubpop.presenters.PullRequestsPresenter;
import githubpop.natanximenes.com.br.githubpop.presenters.PullRequestsPresenterImpl;
import githubpop.natanximenes.com.br.githubpop.utils.EndlessRecyclerViewScrollListener;
import githubpop.natanximenes.com.br.githubpop.utils.PullClickListener;

public class PullRequestsActivity extends AppCompatActivity implements PullRequestsView, PullClickListener {
    @BindView(R.id.pullRequests_progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.pullRequests_pulls_list)
    RecyclerView pullList;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.pullRequests_view)
    RelativeLayout relativeLayout;
    @BindView(R.id.toolbar_back_button)
    LinearLayout backButton;
    String owner;
    String repoName;
    int currentPage;
    PullRequestsPresenter pullRequestsPresenter;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;
    boolean loadMore;
    List<Pull> pullRequests;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_requests);
        ButterKnife.bind(this);
        owner = getIntent().getExtras().getString("owner");
        repoName = getIntent().getExtras().getString("repoName");
        currentPage = 1;
        backButton.setVisibility(View.VISIBLE);
        toolbarTitle.setText(repoName);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        pullList.setLayoutManager(layoutManager);
        loadMore = false;
        pullRequests = new ArrayList<>();

        pullRequestsPresenter = new PullRequestsPresenterImpl(this);
        pullRequestsPresenter.attachToView();

        if (savedInstanceState == null) {
            pullRequestsPresenter.getPullRequests(owner, repoName, currentPage);
        } else {
            pullRequests = savedInstanceState.getParcelableArrayList("pulls");
            adapter = new PullRequestsAdapter(pullRequests, getApplicationContext(), this);
            pullList.setAdapter(adapter);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        pullRequestsPresenter.attachToView();
        pullList.addOnScrollListener(new EndlessRecyclerViewScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadMore = true;
                pullRequestsPresenter.getPullRequests(owner, repoName, page);
                currentPage = page;
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList("pulls", (ArrayList<? extends Parcelable>) pullRequests);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        super.onStop();
        pullRequestsPresenter.detachFromView();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showPulls(List<Pull> pulls) {
        pullRequests.addAll(pulls);
        if (!loadMore) {
            adapter = new PullRequestsAdapter(pullRequests, getApplicationContext(), this);
            pullList.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    @OnClick(R.id.toolbar_back_button)
    public void back() {
        finish();
    }

    @Override
    public void showRetryOption() {
        Snackbar.make(relativeLayout, getString(R.string.retry_message), Snackbar.LENGTH_INDEFINITE)
                .setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pullRequestsPresenter.getPullRequests(owner, repoName, currentPage);
                    }
                }).show();
    }

    @Override
    public void onPullClickListener(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        Intent chooser = Intent.createChooser(intent, "Choose a browser to complete action");
        startActivity(chooser);
    }
}
