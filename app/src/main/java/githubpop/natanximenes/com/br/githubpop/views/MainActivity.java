package githubpop.natanximenes.com.br.githubpop.views;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import githubpop.natanximenes.com.br.githubpop.R;
import githubpop.natanximenes.com.br.githubpop.adapters.RepoListAdapter;
import githubpop.natanximenes.com.br.githubpop.model.Repository;
import githubpop.natanximenes.com.br.githubpop.presenters.MainPresenter;
import githubpop.natanximenes.com.br.githubpop.presenters.MainPresenterImpl;
import githubpop.natanximenes.com.br.githubpop.utils.EndlessRecyclerViewScrollListener;
import githubpop.natanximenes.com.br.githubpop.utils.RepoClickListener;

public class MainActivity extends AppCompatActivity implements MainView, RepoClickListener {
    @BindView(R.id.main_progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.main_repo_list)
    RecyclerView repoList;
    @BindView(R.id.activity_main)
    RelativeLayout relativeLayout;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    MainPresenter mainPresenter;
    List<Repository> repos;
    boolean loadedMore;
    int currentPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        repos = new ArrayList<>();
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        repoList.setLayoutManager(layoutManager);
        toolbarTitle.setText(getString(R.string.app_name));
        mainPresenter = new MainPresenterImpl(this);
        loadedMore = false;
        currentPage = 1;
        mainPresenter.attachToView();
        if (savedInstanceState == null) {
            mainPresenter.searchPopRepos(currentPage);
        } else {
            repos = savedInstanceState.getParcelableArrayList("repos");
            adapter = new RepoListAdapter(repos, getApplicationContext(), this);
            repoList.setAdapter(adapter);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList("repos", (ArrayList<? extends Parcelable>) repos);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mainPresenter.attachToView();
        repoList.addOnScrollListener(new EndlessRecyclerViewScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadedMore = true;
                mainPresenter.searchPopRepos(page);
                currentPage = page;
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        mainPresenter.detachFromView();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showRepos(List<Repository> listRepos) {
        repos.addAll(listRepos);
        if (!loadedMore) {
            adapter = new RepoListAdapter(repos, getApplicationContext(), this);
            repoList.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void showRetryOption() {
        Snackbar.make(relativeLayout, getString(R.string.retry_message), Snackbar.LENGTH_INDEFINITE)
                .setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mainPresenter.searchPopRepos(currentPage);
                    }
                }).show();

    }

    @Override
    public void onClickRepoListener(String owner, String repoName) {
        Intent intent = new Intent(this, PullRequestsActivity.class);
        intent.putExtra("owner", owner);
        intent.putExtra("repoName", repoName);
        startActivity(intent);
    }
}
