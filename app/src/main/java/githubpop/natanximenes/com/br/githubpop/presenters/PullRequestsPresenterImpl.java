package githubpop.natanximenes.com.br.githubpop.presenters;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import githubpop.natanximenes.com.br.githubpop.events.PullRequestsEvent;
import githubpop.natanximenes.com.br.githubpop.events.UnexpectedErrorEvent;
import githubpop.natanximenes.com.br.githubpop.interactors.GithubPullRequestInteractor;
import githubpop.natanximenes.com.br.githubpop.interactors.GithubPullRequestInteractorImpl;
import githubpop.natanximenes.com.br.githubpop.views.PullRequestsView;

/**
 * Created by natanximenes on 9/26/16.
 */
public class PullRequestsPresenterImpl implements PullRequestsPresenter {
    EventBus bus;
    PullRequestsView pullRequestsView;
    GithubPullRequestInteractor githubPullRequestInteractor;

    public PullRequestsPresenterImpl(PullRequestsView pullRequestsView) {
        this.pullRequestsView = pullRequestsView;
        bus = EventBus.getDefault();
        githubPullRequestInteractor = new GithubPullRequestInteractorImpl();
    }

    public PullRequestsPresenterImpl() {

    }

    @Override
    public void getPullRequests(String owner, String repoName, int page) {
        pullRequestsView.showProgress();
        githubPullRequestInteractor.retrieveRepoPullRequests(owner, repoName, page);
    }

    @Override
    public void attachToView() {
        if (!bus.isRegistered(this)) {
            bus.register(this);
        }
    }

    @Override
    public void detachFromView() {
        bus.unregister(this);
    }

    @Subscribe
    public void onEvent(PullRequestsEvent pullRequestsEvent) {
        pullRequestsView.hideProgress();
        pullRequestsView.showPulls(pullRequestsEvent.getPulls());
    }

    @Subscribe
    public void onEvent(UnexpectedErrorEvent errorEvent) {
        pullRequestsView.hideProgress();
        pullRequestsView.showRetryOption();
    }
}
