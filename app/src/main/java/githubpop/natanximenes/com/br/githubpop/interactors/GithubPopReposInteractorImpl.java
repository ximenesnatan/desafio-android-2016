package githubpop.natanximenes.com.br.githubpop.interactors;

import org.greenrobot.eventbus.EventBus;

import githubpop.natanximenes.com.br.githubpop.events.RepositoriesEvent;
import githubpop.natanximenes.com.br.githubpop.events.UnexpectedErrorEvent;
import githubpop.natanximenes.com.br.githubpop.model.SearchResult;
import githubpop.natanximenes.com.br.githubpop.service.Api;
import githubpop.natanximenes.com.br.githubpop.service.GithubRepoService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by natanximenes on 9/22/16.
 */
public class GithubPopReposInteractorImpl implements GithubPopReposInteractor {
    private final String TAG = getClass().getCanonicalName();
    EventBus bus;
    GithubRepoService githubRepoService;
    Api gitHub;

    public GithubPopReposInteractorImpl() {
        this.bus = EventBus.getDefault();
    }

    @Override
    public void findPopRepositories(int page) {
        gitHub = Api.getApiInstance();
        githubRepoService = gitHub.getApi().create(GithubRepoService.class);

        githubRepoService.showPopRepos(page).enqueue(new Callback<SearchResult>() {
            @Override
            public void onResponse(Call<SearchResult> call, Response<SearchResult> response) {
                if (response.isSuccessful()) {
                    SearchResult searchResult = response.body();
                    bus.post(new RepositoriesEvent(searchResult.getRepositories()));
                } else {
                    bus.post(new UnexpectedErrorEvent());
                }
            }

            @Override
            public void onFailure(Call<SearchResult> call, Throwable t) {
                bus.post(new UnexpectedErrorEvent());
            }
        });
    }
}
