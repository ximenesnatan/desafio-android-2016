package githubpop.natanximenes.com.br.githubpop.presenters;

/**
 * Created by natanximenes on 9/26/16.
 */

public interface PullRequestsPresenter {
    void getPullRequests(String owner, String repoName, int page);
    void attachToView();
    void detachFromView();
}
