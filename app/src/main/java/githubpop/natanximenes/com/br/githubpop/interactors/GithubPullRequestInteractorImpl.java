package githubpop.natanximenes.com.br.githubpop.interactors;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import githubpop.natanximenes.com.br.githubpop.events.PullRequestsEvent;
import githubpop.natanximenes.com.br.githubpop.events.UnexpectedErrorEvent;
import githubpop.natanximenes.com.br.githubpop.model.Pull;
import githubpop.natanximenes.com.br.githubpop.service.Api;
import githubpop.natanximenes.com.br.githubpop.service.GithubPullRequestService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by natanximenes on 9/26/16.
 */
public class GithubPullRequestInteractorImpl implements GithubPullRequestInteractor {
    EventBus bus;
    Api gitHub;
    GithubPullRequestService githubPullRequestService;

    public GithubPullRequestInteractorImpl() {
        this.bus = EventBus.getDefault();
    }

    @Override
    public void retrieveRepoPullRequests(String owner, String repoName, int page) {
        gitHub = Api.getApiInstance();
        githubPullRequestService = gitHub.getApi().create(GithubPullRequestService.class);

        githubPullRequestService.getRepoPulls(owner, repoName, page).enqueue(new Callback<List<Pull>>() {
            @Override
            public void onResponse(Call<List<Pull>> call, Response<List<Pull>> response) {
                if (response.isSuccessful()) {
                    bus.post(new PullRequestsEvent(response.body()));
                } else {
                    bus.post(new UnexpectedErrorEvent());
                }
            }

            @Override
            public void onFailure(Call<List<Pull>> call, Throwable t) {
                bus.post(new UnexpectedErrorEvent());
            }
        });
    }
}
