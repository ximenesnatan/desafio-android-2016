package githubpop.natanximenes.com.br.githubpop.interactors;

/**
 * Created by natanximenes on 9/26/16.
 */

public interface GithubPullRequestInteractor {
    void retrieveRepoPullRequests(String owner, String repoName, int page);
}
