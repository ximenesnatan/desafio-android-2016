package githubpop.natanximenes.com.br.githubpop.adapters;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import githubpop.natanximenes.com.br.githubpop.R;
import githubpop.natanximenes.com.br.githubpop.model.Pull;
import githubpop.natanximenes.com.br.githubpop.utils.PullClickListener;

/**
 * Created by natanximenes on 9/27/16.
 */
public class PullRequestsAdapter extends RecyclerView.Adapter<PullRequestsAdapter.ViewHolder> {
    List<Pull> pulls;
    Context context;
    PullClickListener pullClickListener;

    public PullRequestsAdapter(List<Pull> pulls, Context context, PullClickListener pullClickListener) {
        this.pulls = pulls;
        this.context = context;
        this.pullClickListener = pullClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.pull_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Pull pull = pulls.get(position);
        Spanned body;

        if (pull.getOwner().getName() != null) {
            holder.fullUserName.setText(pull.getOwner().getName());
        }
        String date = String.format(context.getString(R.string.pull_created_date), pull.getDate());
        holder.createdAt.setText(date);
        holder.nickname.setText(pull.getOwner().getLogin());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            body = Html.fromHtml(pull.getBody(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            body = Html.fromHtml(pull.getBody());
        }
        holder.pullDescription.setText(body);
        holder.pullTitle.setText(pull.getTitle());
        Picasso.with(context).load(pull.getOwner().getAvatarUrl()).into(holder.userAvatar);
        holder.pullClickListener = pullClickListener;
        holder.url = pull.getUrl();
    }

    @Override
    public int getItemCount() {
        return pulls.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.pullItem_card)
        CardView card;
        @BindView(R.id.pullItem_created_date)
        TextView createdAt;
        @BindView(R.id.pullItem_full_user_name)
        TextView fullUserName;
        @BindView(R.id.pullItem_nickname)
        TextView nickname;
        @BindView(R.id.pullItem_pull_description)
        TextView pullDescription;
        @BindView(R.id.pullItem_pull_title)
        TextView pullTitle;
        @BindView(R.id.pullItem_user_avatar)
        ImageView userAvatar;
        PullClickListener pullClickListener;
        String url;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.pullItem_card)
        public void pullClicked() {
            pullClickListener.onPullClickListener(url);
        }
    }
}
