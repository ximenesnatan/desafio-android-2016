package githubpop.natanximenes.com.br.githubpop.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import githubpop.natanximenes.com.br.githubpop.utils.DateUtils;

/**
 * Created by natanximenes on 9/26/16.
 */
public class Pull implements Parcelable {
    public static final Creator<Pull> CREATOR = new Creator<Pull>() {
        @Override
        public Pull createFromParcel(Parcel in) {
            return new Pull(in);
        }

        @Override
        public Pull[] newArray(int size) {
            return new Pull[size];
        }
    };
    private String title;
    private String body;
    @SerializedName("html_url")
    private String url;
    @SerializedName("created_at")
    private String date;
    @SerializedName("user")
    private Owner owner;

    protected Pull(Parcel in) {
        title = in.readString();
        body = in.readString();
        url = in.readString();
        date = in.readString();
        owner = (Owner) in.readSerializable();
    }

    public Pull() {

    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public String getUrl() {
        return url;
    }

    public String getDate() {
        return DateUtils.formatDate(date);
    }

    public Owner getOwner() {
        return owner;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(body);
        parcel.writeString(url);
        parcel.writeString(date);
        parcel.writeSerializable(owner);
    }
}
