package githubpop.natanximenes.com.br.githubpop.views;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import githubpop.natanximenes.com.br.githubpop.BuildConfig;
import githubpop.natanximenes.com.br.githubpop.adapters.RepoListAdapter;
import githubpop.natanximenes.com.br.githubpop.model.Repository;
import githubpop.natanximenes.com.br.githubpop.presenters.MainPresenterImpl;
import githubpop.natanximenes.com.br.githubpop.utils.EndlessRecyclerViewScrollListener;

import static junit.framework.Assert.assertTrue;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by natanximenes on 9/25/16.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class MainActivityTest {

    MainActivity activity;

    @Captor
    ArgumentCaptor<EndlessRecyclerViewScrollListener> scrollListenerArgumentCaptor;

    @Before
    public void setUp() throws Exception {
        activity = Robolectric.buildActivity(MainActivity.class).create().visible().get();

    }

    @Test
    public void testShowProgress() throws Exception {
        activity.showProgress();
        assertThat(activity.progressBar.getVisibility()).isEqualTo(View.VISIBLE);
    }

    @Test
    public void hideProgress() throws Exception {
        activity.hideProgress();
        assertThat(activity.progressBar.getVisibility()).isEqualTo(View.GONE);
    }

    @Test
    public void showReposFirstCall() throws Exception {
        activity.loadedMore = false;
        List<Repository> repos = new ArrayList<>();
        repos.add(null);
        activity.repos = mock(repos.getClass());
        activity.repoList = mock(RecyclerView.class);
        activity.showRepos(repos);

        verify(activity.repos).addAll(repos);
        verify(activity.repoList).setAdapter(any(RepoListAdapter.class));
    }

    @Test
    public void showReposSubsequentCalls() throws Exception {
        activity.loadedMore = true;
        List<Repository> repos = new ArrayList<>();
        repos.add(null);
        activity.repos = mock(repos.getClass());
        activity.adapter = mock(RepoListAdapter.class);
        activity.showRepos(repos);

        verify(activity.repos).addAll(repos);
        verify(activity.adapter).notifyDataSetChanged();
    }

    @Test
    public void validatePresenterWasAttached() throws Exception {
        activity.mainPresenter = mock(MainPresenterImpl.class);

        activity.onStart();

        verify(activity.mainPresenter).attachToView();
    }

    @Test
    public void validatePresenterWasDettached() throws Exception {
        activity.mainPresenter = mock(MainPresenterImpl.class);

        activity.onStop();

        verify(activity.mainPresenter).detachFromView();
    }

    @Test
    public void checkOnLoadMore() throws Exception {
        ArgumentCaptor<EndlessRecyclerViewScrollListener> scrollListenerArgumentCaptor =
                ArgumentCaptor.forClass(EndlessRecyclerViewScrollListener.class);
        int page = 2;
        int totalItemsCount = 60;
        activity.repoList = mock(RecyclerView.class);
        activity.mainPresenter = mock(MainPresenterImpl.class);

        activity.onStart();

        verify(activity.repoList).addOnScrollListener(scrollListenerArgumentCaptor.capture());
        scrollListenerArgumentCaptor.getValue().onLoadMore(page, totalItemsCount);
        assertThat(activity.loadedMore).isEqualTo(true);
        verify(activity.mainPresenter).searchPopRepos(page);
        assertThat(activity.currentPage).isEqualTo(page);
    }

    @Test
    public void checkRepoItemClicked() throws Exception {
        String owner = "owner";
        String repoName = "repoName";
        activity.onClickRepoListener(owner, repoName);
        Intent expectedIntent = new Intent(activity,PullRequestsActivity.class);
        expectedIntent.putExtra("owner",owner);
        expectedIntent.putExtra("repoName",repoName);
        assertTrue(shadowOf(activity).getNextStartedActivity().filterEquals(expectedIntent));
    }
}