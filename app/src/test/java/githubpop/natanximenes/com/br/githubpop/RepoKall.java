package githubpop.natanximenes.com.br.githubpop;

import java.io.IOException;

import githubpop.natanximenes.com.br.githubpop.model.SearchResult;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by natanximenes on 9/24/16.
 */

public class RepoKall implements Call<SearchResult> {

    @Override
    public Response<SearchResult> execute() throws IOException {
        return null;
    }

    @Override
    public void enqueue(Callback<SearchResult> callback) {

    }

    @Override
    public boolean isExecuted() {
        return false;
    }

    @Override
    public void cancel() {

    }

    @Override
    public boolean isCanceled() {
        return false;
    }

    @Override
    public Call<SearchResult> clone() {
        return null;
    }

    @Override
    public Request request() {
        return null;
    }
}
