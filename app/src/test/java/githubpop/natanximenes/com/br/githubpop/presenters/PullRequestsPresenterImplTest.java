package githubpop.natanximenes.com.br.githubpop.presenters;

import org.greenrobot.eventbus.EventBus;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import githubpop.natanximenes.com.br.githubpop.events.PullRequestsEvent;
import githubpop.natanximenes.com.br.githubpop.events.UnexpectedErrorEvent;
import githubpop.natanximenes.com.br.githubpop.interactors.GithubPullRequestInteractor;
import githubpop.natanximenes.com.br.githubpop.model.Pull;
import githubpop.natanximenes.com.br.githubpop.views.PullRequestsView;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by natanximenes on 9/29/16.
 */
public class PullRequestsPresenterImplTest {
    PullRequestsPresenterImpl pullRequestsPresenter;

    @Before
    public void setUp() throws Exception {
        pullRequestsPresenter = new PullRequestsPresenterImpl();
        pullRequestsPresenter.bus = mock(EventBus.class);
        pullRequestsPresenter.pullRequestsView = mock(PullRequestsView.class);
        pullRequestsPresenter.githubPullRequestInteractor = mock(GithubPullRequestInteractor.class);
    }

    @Test
    public void toGetPullRequests() throws Exception {
        String owner = "owner";
        String repoName = "repoName";
        int page = 10;

        pullRequestsPresenter.getPullRequests(owner, repoName, page);

        verify(pullRequestsPresenter.pullRequestsView).showProgress();
        verify(pullRequestsPresenter.githubPullRequestInteractor)
                .retrieveRepoPullRequests(owner, repoName, page);
    }

    @Test
    public void wasAttachedToView() throws Exception {
        when(pullRequestsPresenter.bus.isRegistered(pullRequestsPresenter)).thenReturn(false);

        pullRequestsPresenter.attachToView();

        verify(pullRequestsPresenter.bus).register(pullRequestsPresenter);
    }

    @Test
    public void wasDetachedFromView() throws Exception {
        pullRequestsPresenter.detachFromView();

        verify(pullRequestsPresenter.bus).unregister(pullRequestsPresenter);
    }

    @Test
    public void pullsRetrieved() throws Exception {
        List<Pull> pulls = new ArrayList<>();
        pulls.add(new Pull());
        PullRequestsEvent pullRequestsEvent = new PullRequestsEvent(pulls);

        pullRequestsPresenter.onEvent(pullRequestsEvent);

        verify(pullRequestsPresenter.pullRequestsView).hideProgress();
        verify(pullRequestsPresenter.pullRequestsView).showPulls(pullRequestsEvent.getPulls());

    }

    @Test
    public void handleUnexpectedError() throws Exception {
        UnexpectedErrorEvent unexpectedErrorEvent = new UnexpectedErrorEvent();

        pullRequestsPresenter.onEvent(unexpectedErrorEvent);

        verify(pullRequestsPresenter.pullRequestsView).hideProgress();
        verify(pullRequestsPresenter.pullRequestsView).showRetryOption();
    }

}