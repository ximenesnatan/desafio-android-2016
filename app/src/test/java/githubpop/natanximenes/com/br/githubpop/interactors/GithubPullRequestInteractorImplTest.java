package githubpop.natanximenes.com.br.githubpop.interactors;

import org.greenrobot.eventbus.EventBus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import githubpop.natanximenes.com.br.githubpop.PullKall;
import githubpop.natanximenes.com.br.githubpop.events.PullRequestsEvent;
import githubpop.natanximenes.com.br.githubpop.events.UnexpectedErrorEvent;
import githubpop.natanximenes.com.br.githubpop.model.Pull;
import githubpop.natanximenes.com.br.githubpop.service.Api;
import githubpop.natanximenes.com.br.githubpop.service.GithubPullRequestService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by natanximenes on 9/29/16.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({EventBus.class, Api.class, Retrofit.class, Response.class})
public class GithubPullRequestInteractorImplTest {
    GithubPullRequestInteractorImpl githubPullRequestInteractor;
    Api api;
    @Captor
    ArgumentCaptor<Callback<List<Pull>>> pullsResultCallback;
    Call<List<Pull>> pullCall;
    GithubPullRequestService githubPullRequestService;
    int page;
    String owner;
    String repoName;

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(EventBus.class);
        when(EventBus.getDefault()).thenReturn(mock(EventBus.class));
        githubPullRequestInteractor = new GithubPullRequestInteractorImpl();

        api = mock(Api.class);
        pullCall = mock(PullKall.class);
        githubPullRequestService = mock(GithubPullRequestService.class);
        repoName = "repoName";
        owner = "owner";
        page = 10;
        githubPullRequestInteractor.gitHub = api;
        githubPullRequestInteractor.githubPullRequestService = githubPullRequestService;

        PowerMockito.mockStatic(EventBus.class);
        PowerMockito.mockStatic(Api.class);

        when(Api.getApiInstance()).thenReturn(api);
        when(githubPullRequestInteractor.gitHub.getApi()).thenReturn(PowerMockito.mock(Retrofit.class));
        PowerMockito.when(githubPullRequestInteractor.gitHub.getApi().create(GithubPullRequestService.class))
                .thenReturn(githubPullRequestService);
        when(githubPullRequestInteractor.githubPullRequestService.getRepoPulls(owner, repoName, page))
                .thenReturn(pullCall);
    }

    @Test
    public void successfulPullRequestsRetrieving() throws Exception {
        githubPullRequestInteractor.retrieveRepoPullRequests(owner, repoName, page);

        verify(githubPullRequestInteractor.githubPullRequestService.getRepoPulls(owner, repoName, page))
                .enqueue(pullsResultCallback.capture());
        List<Pull> mockPulls = new ArrayList<>();
        mockPulls.add(mock(Pull.class));
        pullsResultCallback.getValue().onResponse(pullCall, Response.success(mockPulls));

        verify(githubPullRequestInteractor.bus).post(any(PullRequestsEvent.class));
    }

    @Test
    public void failurePullRequestsRetrieving() throws Exception {
        githubPullRequestInteractor.retrieveRepoPullRequests(owner, repoName, page);

        verify(githubPullRequestInteractor.githubPullRequestService.getRepoPulls(owner, repoName, page))
                .enqueue(pullsResultCallback.capture());
        pullsResultCallback.getValue().onFailure(pullCall, mock(Throwable.class));

        verify(githubPullRequestInteractor.bus).post(any(UnexpectedErrorEvent.class));
    }

    @Test
    public void notSuccefulPullRequestsRetrieving() throws Exception {
        githubPullRequestInteractor.retrieveRepoPullRequests(owner, repoName, page);

        List<Pull> mockPulls = new ArrayList<>();
        mockPulls.add(mock(Pull.class));
        //Doing a trick to mock the final generic Response class
        Response<List<Pull>> response = Response.success(mockPulls);
        Response<List<Pull>> mockedResponse = PowerMockito.mock(response.getClass());
        when(mockedResponse.isSuccessful()).thenReturn(false);

        verify(githubPullRequestInteractor.githubPullRequestService.getRepoPulls(owner, repoName, page))
                .enqueue(pullsResultCallback.capture());
        pullsResultCallback.getValue().onResponse(pullCall, mockedResponse);

        verify(githubPullRequestInteractor.bus).post(any(UnexpectedErrorEvent.class));
    }
}