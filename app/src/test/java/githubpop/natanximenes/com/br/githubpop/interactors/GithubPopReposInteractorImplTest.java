package githubpop.natanximenes.com.br.githubpop.interactors;

import org.greenrobot.eventbus.EventBus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import githubpop.natanximenes.com.br.githubpop.RepoKall;
import githubpop.natanximenes.com.br.githubpop.events.RepositoriesEvent;
import githubpop.natanximenes.com.br.githubpop.events.UnexpectedErrorEvent;
import githubpop.natanximenes.com.br.githubpop.model.SearchResult;
import githubpop.natanximenes.com.br.githubpop.service.Api;
import githubpop.natanximenes.com.br.githubpop.service.GithubRepoService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by natanximenes on 9/24/16.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({EventBus.class, Api.class, Retrofit.class, Response.class})
public class GithubPopReposInteractorImplTest {
    GithubPopReposInteractorImpl githubPopReposInteractor;
    Api api;
    GithubRepoService githubRepoService;
    @Captor
    ArgumentCaptor<Callback<SearchResult>> searchResultCallBack;
    int page;
    Call<SearchResult> call;

    @Before
    public void setUp() throws Exception {
        //mocking eventbus, which was always called at the constructor
        PowerMockito.mockStatic(EventBus.class);
        when(EventBus.getDefault()).thenReturn(mock(EventBus.class));
        githubPopReposInteractor = new GithubPopReposInteractorImpl();

        //Initializing variables
        api = mock(Api.class);
        page = 10;
        call = mock(RepoKall.class);
        githubRepoService = mock(GithubRepoService.class);

        //Mocking class attributes
        githubPopReposInteractor.gitHub = api;
        githubPopReposInteractor.githubRepoService = githubRepoService;

        //Mocking necessary static methods of classes
        PowerMockito.mockStatic(EventBus.class);
        PowerMockito.mockStatic(Api.class);

        //Mocking necessary methods returns
        when(Api.getApiInstance()).thenReturn(api);
        when(githubPopReposInteractor.gitHub.getApi()).thenReturn(PowerMockito.mock(Retrofit.class));
        PowerMockito.when(githubPopReposInteractor.gitHub.getApi().create(GithubRepoService.class))
                .thenReturn(githubRepoService);
        when(githubPopReposInteractor.githubRepoService.showPopRepos(page)).thenReturn(call);
    }

    @Test
    public void successfullFindingPopRepositories() throws Exception {
        githubPopReposInteractor.findPopRepositories(page);

        verify(githubPopReposInteractor.githubRepoService.showPopRepos(page)).enqueue(searchResultCallBack.capture());
        searchResultCallBack.getValue().onResponse(call, Response.success(mock(SearchResult.class)));

        verify(githubPopReposInteractor.bus).post(any(RepositoriesEvent.class));
    }

    @Test
    public void failureFindindPopRepositories() throws Exception {
        githubPopReposInteractor.findPopRepositories(page);

        verify(githubPopReposInteractor.githubRepoService.showPopRepos(page)).enqueue(searchResultCallBack.capture());
        searchResultCallBack.getValue().onFailure(call, mock(Throwable.class));

        verify(githubPopReposInteractor.bus).post(any(UnexpectedErrorEvent.class));

    }

    @Test
    public void notSuccessfullFindingPopRepositories() throws Exception {
        githubPopReposInteractor.findPopRepositories(page);

        //Doing a trick to mock the final generic Response class
        Response<SearchResult> response = Response.success(mock(SearchResult.class));
        Response<SearchResult> mockedResponse = PowerMockito.mock(response.getClass());
        when(mockedResponse.isSuccessful()).thenReturn(false);

        verify(githubPopReposInteractor.githubRepoService.showPopRepos(page)).enqueue(searchResultCallBack.capture());
        searchResultCallBack.getValue().onResponse(call, mockedResponse);

        verify(githubPopReposInteractor.bus).post(any(RepositoriesEvent.class));

    }

}