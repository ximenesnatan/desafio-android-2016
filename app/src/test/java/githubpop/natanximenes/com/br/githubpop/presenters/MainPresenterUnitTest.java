package githubpop.natanximenes.com.br.githubpop.presenters;

import org.greenrobot.eventbus.EventBus;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import githubpop.natanximenes.com.br.githubpop.events.RepositoriesEvent;
import githubpop.natanximenes.com.br.githubpop.events.UnexpectedErrorEvent;
import githubpop.natanximenes.com.br.githubpop.interactors.GithubPopReposInteractor;
import githubpop.natanximenes.com.br.githubpop.model.Repository;
import githubpop.natanximenes.com.br.githubpop.views.MainView;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by natanximenes on 9/24/16.
 */

public class MainPresenterUnitTest {
    MainPresenterImpl mainPresenter;

    @Before
    public void setup() throws Exception {
        mainPresenter = new MainPresenterImpl();
        mainPresenter.mainView = mock(MainView.class);
        mainPresenter.githubPopReposInteractor = mock(GithubPopReposInteractor.class);
        mainPresenter.bus = mock(EventBus.class);
    }

    @Test
    public void wasAttachedToView() throws Exception {
        when(mainPresenter.bus.isRegistered(any(MainPresenterImpl.class))).thenReturn(false);
        mainPresenter.attachToView();

        verify(mainPresenter.bus).register(mainPresenter);
    }

    @Test
    public void wasDettachedToView() throws Exception {
        mainPresenter.detachFromView();

        verify(mainPresenter.bus).unregister(mainPresenter);
    }

    @Test
    public void toFindRepositories() throws Exception {
        int page = 10;

        mainPresenter.searchPopRepos(page);

        verify(mainPresenter.mainView).showProgress();
        verify(mainPresenter.githubPopReposInteractor).findPopRepositories(page);
    }

    @Test
    public void repositoriesRetrieved() throws Exception {
        List<Repository> repositories = new ArrayList<>();
        repositories.add(new Repository());
        RepositoriesEvent repositoriesEvent = new RepositoriesEvent(repositories);

        mainPresenter.onEvent(repositoriesEvent);

        verify(mainPresenter.mainView).hideProgress();
        verify(mainPresenter.mainView).showRepos(repositoriesEvent.getRepositories());
    }

    @Test
    public void handleUnexpectedError() {
        UnexpectedErrorEvent erroEvent = new UnexpectedErrorEvent();

        mainPresenter.onEvent(erroEvent);

        verify(mainPresenter.mainView).hideProgress();
        verify(mainPresenter.mainView).showRetryOption();
    }
}
