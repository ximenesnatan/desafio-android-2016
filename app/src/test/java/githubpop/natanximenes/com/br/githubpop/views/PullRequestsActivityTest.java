package githubpop.natanximenes.com.br.githubpop.views;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.util.ActivityController;

import java.util.ArrayList;
import java.util.List;

import githubpop.natanximenes.com.br.githubpop.BuildConfig;
import githubpop.natanximenes.com.br.githubpop.adapters.PullRequestsAdapter;
import githubpop.natanximenes.com.br.githubpop.model.Pull;
import githubpop.natanximenes.com.br.githubpop.presenters.PullRequestsPresenter;
import githubpop.natanximenes.com.br.githubpop.utils.EndlessRecyclerViewScrollListener;

import static junit.framework.Assert.assertTrue;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by natanximenes on 9/29/16.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class PullRequestsActivityTest {
    PullRequestsActivity pullRequestsActivity;

    @Before
    public void setUp() throws Exception {
        String owner = "owner";
        String repoName = "repoName";
        Intent intent = new Intent(mock(MainActivity.class), PullRequestsActivity.class);
        intent.putExtra("owner", owner);
        intent.putExtra("repoName", repoName);
        pullRequestsActivity = Robolectric.buildActivity(PullRequestsActivity.class).withIntent(intent).create().visible().get();
    }

    @Test
    public void showProgress() throws Exception {
        pullRequestsActivity.showProgress();
        assertThat(pullRequestsActivity.progressBar.getVisibility()).isEqualTo(View.VISIBLE);
    }

    @Test
    public void hideProgress() throws Exception {
        pullRequestsActivity.hideProgress();
        assertThat(pullRequestsActivity.progressBar.getVisibility()).isEqualTo(View.GONE);
    }

    @Test
    public void showPullsFirstCall() throws Exception {
        List<Pull> mockPulls = new ArrayList<>();
        mockPulls.add(null);
        pullRequestsActivity.loadMore = false;
        pullRequestsActivity.pullRequests = mock(mockPulls.getClass());
        pullRequestsActivity.pullList = mock(RecyclerView.class);
        pullRequestsActivity.showPulls(mockPulls);

        verify(pullRequestsActivity.pullRequests).addAll(mockPulls);
        verify(pullRequestsActivity.pullList).setAdapter(any(PullRequestsAdapter.class));
    }

    @Test
    public void showPullsSubsequentCall() throws Exception {
        List<Pull> mockPulls = new ArrayList<>();
        mockPulls.add(null);
        pullRequestsActivity.loadMore = true;
        pullRequestsActivity.pullRequests = mock(mockPulls.getClass());
        pullRequestsActivity.adapter = mock(PullRequestsAdapter.class);
        pullRequestsActivity.showPulls(mockPulls);

        verify(pullRequestsActivity.pullRequests).addAll(mockPulls);
        verify(pullRequestsActivity.adapter).notifyDataSetChanged();

    }

    @Test
    public void validatePresenterWasAttached() throws Exception {
        pullRequestsActivity.pullRequestsPresenter = mock(PullRequestsPresenter.class);

        pullRequestsActivity.onStart();

        verify(pullRequestsActivity.pullRequestsPresenter).attachToView();
    }

    @Test
    public void validatePresenterWasDetached() throws Exception {
        pullRequestsActivity.pullRequestsPresenter = mock(PullRequestsPresenter.class);

        pullRequestsActivity.onStop();

        verify(pullRequestsActivity.pullRequestsPresenter).detachFromView();

    }

    @Test
    public void checkOnLoadMore() throws Exception {
        ArgumentCaptor<EndlessRecyclerViewScrollListener> scrollListenerArgumentCaptor =
                ArgumentCaptor.forClass(EndlessRecyclerViewScrollListener.class);
        pullRequestsActivity.pullRequestsPresenter = mock(PullRequestsPresenter.class);
        pullRequestsActivity.pullList = mock(RecyclerView.class);
        int page = 2;
        int totalItemsCount = 60;
        String owner = "owner";
        String repoName = "repoName";
        pullRequestsActivity.onStart();

        verify(pullRequestsActivity.pullList).addOnScrollListener(scrollListenerArgumentCaptor.capture());
        scrollListenerArgumentCaptor.getValue().onLoadMore(page, totalItemsCount);

        assertThat(pullRequestsActivity.loadMore).isEqualTo(true);
        verify(pullRequestsActivity.pullRequestsPresenter).getPullRequests(owner, repoName, page);
        assertThat(pullRequestsActivity.currentPage).isEqualTo(page);
    }

    @Test
    public void checkPullItemClicked() throws Exception {
        String url = "http://www.somepull.com";
        pullRequestsActivity.onPullClickListener(url);
        Intent browser = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        Intent expectedIntent = Intent.createChooser(browser, "Choose a browser to complete action");
        assertTrue(shadowOf(pullRequestsActivity).getNextStartedActivity().filterEquals(expectedIntent));
    }

}